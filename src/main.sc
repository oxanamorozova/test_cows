require: slotfilling/slotFilling.sc
  module = sys.zb-common

require: common.js
    module = sys.zb-common  
  
require: functions.js
  

theme: /

    state: Start || modal = true
        q!: $regex</start>
        intent!: /lets_play
        script:
            $session = {};
        a: Предлагаю игру "Быки и коровы". Я загадаю число из четырех неповторяющихся цифр (например, 3247), ты будешь его отгадывать. Для каждой попытки я буду говорить, сколько цифр угадано без совпадения с их позициями в загаданном числе (то есть количество коров) и сколько угадано вплоть до позиции в загаданном числе (то есть количество быков). Поиграем?
        go!: /Start/Agree?

        state: Agree?
        
            state: Yes
                intent: /yes
                intent: /lets_play
                go!: /Game

            state: No
                intent: /no
                intent: /dont_know
                intent: /stop
                a: Ну и ладно! Если передумаешь, то скажи "давай поиграем":)
                
            
            state: Number
                intent: /number
                intent: /float
                a: Я еще не загадал:). Загадываю?
                go!: /Start/Agree?
                
            state: NoMatch
                event: noMatch
                a: Прости, но я тебя не понял.  

    state: Game
        script:
            var number = RandNumber();
            $session.number = number;
        a: Я загадал число! Назови положительное целое четырехзначное число из неповторяющихся цифр, записанное без пробелов.
        go!: /Check


    state: Check

        state: Number
            intent: /number
            script:
                var userAnswer = $parseTree._Number;
                userAnswer = userAnswer.toString();
                $session.userAnswer = userAnswer;
                $reactions.answer("Твой ответ: " + $session.userAnswer + ".");
                var cows = 0;
                var bulls = 0;
                $session.cows = cows;
                $session.bulls = bulls;
                var counterEntry = 0; //счетчик вхождений каждой из цифр числа пользователя в это число
                var floatNegative = false; //проверка на нецелое/отрицательное число польователя
                for (var i = 0; i < $session.userAnswer.length; i++) {
                    if ($session.userAnswer[i] == "." || $session.userAnswer[i] == "," || $session.userAnswer[i] == "-") {
                        floatNegative = true;
                        break;
                    }
                }  
                if (floatNegative == true) {
                        $reactions.answer("Такой вариант не подойдет. Я загадал положительное целое четырехзначное число из неповторяющихся цифр.");
                        $reactions.transition("/Check");
                }
                else if ($session.userAnswer.length != 4) {
                    $reactions.answer("Это не четырехзначное число. Я загадал четырехзначное число из неповторяющихся цифр.");
                    $reactions.transition("/Check");
                }
                else {
                    //проверка, что пользователь ввел число из 4-х различных цифр
                    for (var i = 0; i < $session.userAnswer.length - 1; i++){
                        for (var j = i + 1; j < $session.userAnswer.length; j++) {
                            if ($session.userAnswer[i] === $session.userAnswer[j]) {
                                counterEntry ++;
                            }
                        }
                    }    
                    if (counterEntry > 0) {
                        $reactions.answer("В этом числе цифры повторяются. Я загадал четырехзначное число из неповторяющихся цифр.");
                        $reactions.transition("/Check");    
                    }
                    else if (userAnswer == $session.number) {
                        $reactions.answer(selectRandomArg("Отгадано! Поиграем еще раз?", "Бинго! Поиграем еще раз?", "Верно! Поиграем еще раз?"));
                        $reactions.transition("/Start/Agree?");
                    }
                    else {
                        for (var i = 0; i < $session.number.length; i++){
                            //считаем быков и коров
                            var numUser = userAnswer[i]; //numUser цифра из числа пользователя
                            var numBot = $session.number.indexOf(numUser); //numBot цифра из загаданного ботом числа
                            if (numBot != -1) {      // если вхождение есть
                                if (numBot == i) {   //если и индекс совпадает
                                    $session.bulls ++;
                                }
                                else {               //только вхождение, индекс не совпал
                                    $session.cows ++; 
                                }
                            }
                        }
                    }
                }    
            if: $session.bulls == 0 && $session.cows == 0
                a: Результат: {{$session.bulls}} быков, {{$session.cows}} коров.
            elseif: $session.bulls == 1 && $session.cows == 0
                a: Результат: {{$session.bulls}} бык, {{$session.cows}} коров.
            elseif: $session.bulls == 0 && $session.cows == 1
                a: Результат: {{$session.bulls}} быков, {{$session.cows}} корова.
            elseif: $session.bulls >= 2 && $session.cows == 0
                a: Результат: {{$session.bulls}} быка, {{$session.cows}} коров.
            elseif: $session.bulls == 0 && $session.cows >= 2
                a: Результат: {{$session.bulls}} быков, {{$session.cows}} коровы.       
            elseif: $session.bulls == 1 && $session.cows == 1
                a: Результат: {{$session.bulls}} бык, {{$session.cows}} корова.
            elseif: $session.bulls >= 2 && $session.cows == 1
                a: Результат: {{$session.bulls}} быка, {{$session.cows}} корова.
            elseif: $session.bulls == 1 && $session.cows >= 2
                a: Результат: {{$session.bulls}} бык, {{$session.cows}} коровы.
            else:
                a: Результат: {{$session.bulls}} быка, {{$session.cows}} коровы.
            go!: /Check
            
            
        state: Float
            intent: /float
            a: Нужно ввести положительное целое четырехзначное число из неповторяющихся цифр.
            go!: /Check 


    state: Not_my_answer
        intent!: /not_my_answer
        a: Нужно ввести без пробелов положительное целое четырехзначное число из неповторяющихся цифр.
        go!: /Check 
        
    state: Dont_know
        intent!: /dont_know
        a: Назови четырехзначное число и скоро узнаешь:).
        go!: /Check     
        
    state: Yourself
        intent!: /yourself
        a: Я знаю число, поэтому не могу назвать его, в этом смысл игры:). Назови четырехзначное число и тоже скоро узнаешь:).
        go!: /Check     

    state: Stop
        intent!: /stop
        a: Ну и ладно! Если передумаешь, то скажи "давай поиграем":). Я загадал число {{$session.number}}.
        go!: /  

    state: NoMatch
        event!: noMatch
        a: Прости, но я тебя не понял.

    state: reset
        q!: reset
        script:
            $session = {};
        go!: /                