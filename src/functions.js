//рандомайзер четырехзначного числа, все значения которого различны

function RandNumber() {
    var arr = [];
    while (arr.length < 4) {
        var n = $jsapi.random(10)
        if (arr.indexOf(n) == -1) {
            arr.push(n)
        }
    } 
    if (arr[0] == 0) {
        arr[0] = arr[3];
        arr[3] = 0;
    }
    return arr.join(''); //вернет строкой, а не массивом
}